let active = true;
// Content script, image replacer
chrome.storage.sync.get(
    {
      activate: true,
    },
    (items) => {
      active = items.activate;
      if (active) {
      // iNikolayev
        const self = {
          images: [
            'https://i.imgur.com/GkGkYE1.jpg',
            'https://i.imgur.com/5gl6sTq.jpg',
            'https://i.imgur.com/isbvW89.jpg',
            'https://i.imgur.com/dSk44iE.jpg',
            'https://i.imgur.com/005kQox.jpg',
            'https://i.imgur.com/Ia9Cw4G.jpg',
            'https://i.imgur.com/yDj3U5k.jpg',
            'https://i.imgur.com/yDj3U5k.jpg',
            'https://i.imgur.com/9os41Ev.jpg',
            'https://i.imgur.com/ySkGSUl.jpg',
          ],
          // Handles all images on page with an interval of time
          handleImages(lstImgs, time) {
            const siteImages = document.getElementsByTagName('img');
            const siteImagesCount = siteImages.length;
            for (let i = 0; i < siteImagesCount; i++) {
              const currentImg = siteImages[i];
              const currentSrc = currentImg.src;
              self.replaceImages(lstImgs, currentImg, currentSrc);
            }
            // Keep replacing
            if (time > 0) {
              setTimeout(() => {
                self.handleImages(lstImgs, time);
              }, time);
            }
          },
          replaceImages(lstImgs, currentImg, currentSrc) {
          // Skip if image is already replaced
            if (!lstImgs.includes(currentSrc)) {
              const imageHeight = currentImg.clientHeight;
              const imageWidth = currentImg.clientWidth;
              // If image loaded
              if (imageHeight > 0 && imageWidth > 0) {
                self.handleImg(currentImg, lstImgs);
              }
            } else {
            // Replace image when loaded
              currentImg.onload = () => {
                if (!lstImgs.includes(currentSrc)) {
                  self.handleImg(currentImg, lstImgs);
                }
              };
            }
          },
          // Replace one image
          handleImg(item, lstImgs) {
            item.onerror = () => {
              self.handleBrokenImg(item, lstImgs);
            };
            self.setRandomImg(item, lstImgs);
          },
          // Set a random image from lstImgs to item
          setRandomImg(item, lstImgs) {
            const imageWidth = item.clientWidth;
            const imageHeight = item.clientHeight;
            item.style.width = `${imageWidth}px`;
            item.style.height = `${imageHeight}px`;
            item.style.objectFit = 'cover';
            item.style.objectPosition = '0 0';
            item.src = lstImgs[Math.floor(Math.random() * lstImgs.length)];
          },
          // Removed broken image from lstImgs, run handleImg on item
          handleBrokenImg(item, lstImgs) {
            const brokenImage = item.src;
            const index = lstImgs.indexOf(brokenImage);
            if (index > -1) {
              lstImgs.splice(index, 1);
            }
            _gaq.push(['_trackEvent', brokenImage, 'broken-image']);
            self.setRandomImg(item, lstImgs);
          },
        };

        // Start when page is load
        document.addEventListener(
            'DOMContentLoaded',
            self.handleImages(self.images, 3000)
        );
      // end iNikolayev
      }
    }
);
// const _gaq = _gaq || [];
// _gaq.push(['_setAccount', 'UA-15665299-28']);
// _gaq.push(['_trackPageview']);
(() => {
  const ga = document.createElement('script');
  ga.type = 'text/javascript';
  ga.async = true;
  ga.src = 'https://ssl.google-analytics.com/ga.js';
  const s = document.getElementsByTagName('script')[0];
  s.parentNode.insertBefore(ga, s);
})();
