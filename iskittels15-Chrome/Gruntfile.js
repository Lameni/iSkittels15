const appFolder = 'app/iskittels15/';
const distFolder = 'dist/iskittels15/';
const scripts = [
  `${appFolder}iskittels15.js`,
  `${appFolder}bg.js`,
  `${appFolder}options/options.js`,
];
const scriptsTemp = [
  'temp/app/iskittels15/iskittels15.js',
  'temp/app/iskittels15/bg.js',
  'temp/app/iskittels15/options/options.js',
];

module.exports = (grunt) => {
  require('jit-grunt')(grunt);

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    htmlmin: {
      dist: {
        options: {
          removeComments: true,
          collapseWhitespace: true,
        },
        files: [
          {
            expand: true,
            cwd: appFolder,
            src: ['options/options.html'],
            dest: distFolder,
          },
        ],
      },
    },
    imagemin: {
      dynamic: {
        files: [
          {
            expand: true,
            cwd: appFolder,
            src: ['**/*.{png,jpg,gif}'],
            dest: distFolder,
          },
        ],
      },
    },
    eslint: {
      target: scripts,
    },
    babel: {
      options: {
        sourceMap: false,
        presets: ['@babel/preset-env'],
      },
      dist: {
        files: [
          {
            expand: true,
            src: scripts,
            dest: 'temp/',
          },
        ],
      },
    },
    uglify: {
      options: {
        stripBanners: true,
        banner:
          '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
          '<%= grunt.template.today("yyyy-mm-dd") %> */',
      },
      dist: {
        files: [
          {
            'dist/iskittels15/iskittels15.js': scriptsTemp[0],
            'dist/iskittels15/bg.js': scriptsTemp[1],
            'dist/iskittels15/options/options.js': scriptsTemp[2],
          },
        ],
      },
    },
    csso: {
      compress: {
        options: {
          report: 'gzip',
        },
        files: {
          'dist/iskittels15/options/options.css': [
            `${appFolder}options/options.css`,
          ],
        },
      },
    },
    clean: ['temp/**/*', 'dist/**', 'dist/.DS_Store'],
    compress: {
      main: {
        options: {
          archive: 'iskittels15.zip',
        },
        files: [
          {
            expand: true,
            cwd: 'dist/',
            src: ['**/*', '_locales/**', 'manifest.json'],
            dest: '',
          },
        ],
      },
    },
    copy: {
      main: {
        files: [
          {
            expand: true,
            cwd: 'app/',
            src: ['_locales/**', 'iskittels15/options/options.css'],
            dest: 'dist/',
          },
        ],
      },
      manifest: {
        src: 'app/manifest.json',
        dest: 'dist/manifest.json',
        options: {
          /*
                    process: function(content, srcpath) {
                      return content.replace('bg.js', 'bg.min.js').replace('iskittels15.js', 'iskittels15.min.js');
                    }
          */
        },
      },
    },
    watch: {
      files: ['<%= eslint.target %>'],
      tasks: ['eslint'],
    },
  });
  grunt.registerTask('default', ['eslint', 'watch']);
  grunt.registerTask('build', [
    'clean',
    'copy',
    'babel',
    'uglify',
    'csso',
    'htmlmin',
    'imagemin',
    'compress',
  ]);
};
